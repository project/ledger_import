<?php

/**
 * @file
 * Ledger Import UI
 */

/*************************************************************************
 * Drupal core hooks
 * ***********************************************************************/

/**
 * Implements hook_menu().
 */
function ledger_import_ui_menu() {

  // Ledger import upload form.
  $items['admin/ledger/import'] = array(
    'title' => 'Import',
    'description' => 'Import data into Ledger',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ledger_import_upload_form'),
    'access arguments' => array('administer ledger'),
    'weight' => 90,
  );
  $items['admin/ledger/import/upload'] = array(
    'title' => 'Import',
    'description' => 'Import data into Ledger',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );

  // Ledger review form.
  $items['admin/ledger/import/review'] = array(
    'title' => 'Review',
    'description' => 'Review imported items',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ledger_import_review_form'),
    'access arguments' => array('administer ledger'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  return $items;
}

/*************************************************************************
 * Form functions
 * ***********************************************************************/

/**
 * Ledger import upload form.
 */
function ledger_import_upload_form($form, &$form_state) {

  // Ask contrib modules for import types.
  $types = module_invoke_all('ledger_import_type_info');

  // Store the import types in the form for later use.
  $form['types'] = array(
    '#type' => 'value',
    '#value' => $types,
  );

  // Build an array of options to select from.
  $options = array();
  if (!empty($types)) {
    foreach ($types as $type => $info) {
      $options[$type] = $info['title'];
    }
  };

  // What kind of file is being imported?
  $form['type'] = array(
    '#type' => 'radios',
    '#title' => t('Import file type'),
    '#description' => t('What type of file would you like to import from?'),
    '#options' => $options,
    '#required' => TRUE,
  );

  // File upload field.
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('File'),
  );

  // Upload button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );

  // Return the form array.
  return $form;
}

/**
 * Validate handler for ledger_import_upload_form().
 */
function ledger_import_upload_form_validate($form, &$form_state) {

  // Retrieve the import types from the form.
  $types = $form_state['values']['types'];

  // Load the list of valid file extensions, based on the file type that was chosen.
  $extensions = implode(' ', $types[$form_state['values']['type']]['filetypes']);

  // Make sure that there are file extensions available, otherwise file_save_upload() won't validate them.
  if (empty($extensions)) {
    form_set_error('type', 'An invalid type was selected');
    return;
  }

  // Validate the file extension and save the uploaded file.
  $file = file_save_upload('file', array(
    'file_validate_extensions' => array($extensions),
  ));

  // If the file passed validation...
  if ($file) {

    // Prepare the destination directory.
    $dir = 'public://feeds/ledger';
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

    // Move the file, into the Drupal file system
    if ($file = file_move($file, $dir)) {

      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t('The file couldn\'t be saved.'));
    }
  }
  else {
    form_set_error('file', t('No file was uploaded.'));
  }
}

/**
 * Submit handler for ledger_import_upload_form().
 */
function ledger_import_upload_form_submit($form, &$form_state) {

  // Retrieve the import types from the form.
  $types = $form_state['values']['types'];

  // Load the file object from storage.
  $file = $form_state['storage']['file'];

  // Make the storage of the file permanent
  $file->status = FILE_STATUS_PERMANENT;

  // Save file status.
  file_save($file);

  // Execute each of the Feeds importers.
  if (!empty($types[$form_state['values']['type']]['importers'])) {
    foreach ($types[$form_state['values']['type']]['importers'] as $importer_id => $type) {

      // If the $type is 'review', then we'll override the processor.
      $override = FALSE;
      if ($type == 'review') {
        $override = TRUE;
      }

      // Import it using the special feeds_review_batch_import() function, which
      // ensures that the Feeds Review processor is used.
      feeds_review_batch_import($importer_id, $file->fid, $override);
    }
  }

  // Make sure nothing is set in storage, so that the redirect will work.
  unset($form_state['storage']);

  // Redirect to the Ledger Import Review page.
  $form_state['redirect'] = 'admin/ledger/import/review';
}

/**
 * Ledger import review form.
 */
function ledger_import_review_form($form, &$form_state) {

  // Get the default Feeds Review form.
  $form = feeds_review_form($form, $form_state);

  // Return the form array.
  return $form;
}

