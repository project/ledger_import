<?php

/**
 * @file
 * Class definition of FeedsLedgerTransactionProcessor.
 */

/**
 * Creates Ledger Transactions from feed items.
 */
class FeedsLedgerTransactionProcessor extends FeedsProcessor {

  /**
   * Define entity type.
   */
  public function entityType() {
    return 'ledger_transaction';
  }

  /**
   * Creates a new transaction in memory and returns it.
   */
  protected function newEntity(FeedsSource $source) {
    $transaction = ledger_transaction_new();
    return $transaction;
  }

  /**
   * Loads an existing transaction.
   *
   * If the update existing method is not FEEDS_UPDATE_EXISTING, a new ledger_transaction will be created, rather than loading the existing one.
   */
  protected function entityLoad(FeedsSource $source, $tid) {
    if ($this->config['update_existing'] == FEEDS_UPDATE_EXISTING) {
      $transaction = ledger_transaction_load($tid);
    }
    else {
      $transaction = ledger_transaction_new();
    }
    return $transaction;
  }

  /**
   * Save a transaction.
   */
  public function entitySave($transaction) {

    // Save the transaction.
    ledger_transaction_save($transaction);

    // If a value was set on the transaction, create two account entries.
    if (!empty($transaction->value)) {

      // Determine the primary and transfer account ids.
      $primary_aid = !empty($this->config['primary_account']) ? $this->config['primary_account'] : ledger_import_unknown_account();
      $transfer_aid = !empty($this->config['transfer_account']) ? $this->config['transfer_account'] : ledger_import_unknown_account();

      // Add the primary account entry.
      $primary_account_entry = ledger_account_entry_new();
      $primary_account_entry->tid = $transaction->tid;
      $primary_account_entry->aid = $primary_aid;
      $primary_account_entry->value = $transaction->value;
      ledger_account_entry_save($primary_account_entry);

      // Add the transfer account entry (reverse the value).
      $transfer_account_entry = ledger_account_entry_new();
      $transfer_account_entry->tid = $transaction->tid;
      $transfer_account_entry->aid = $transfer_aid;
      $primary_account_entry->value = $transaction->value * -1;
      ledger_account_entry_save($transfer_account_entry);
    }
  }

  /**
   * Delete a series of transactions.
   */
  protected function entityDeleteMultiple($tids) {
    ledger_transaction_delete_multiple($tids);
  }

  /**
   * Process the result of the parsing stage.
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {

    // Turn off account entry balance calculation temporarily.
    variable_set('ledger_period_balance_calculate', 0);

    // Run the parent process function.
    parent::process($source, $parser_result);

    // Turn account entry balance calculation back on and recalculate all accounts.
    variable_set('ledger_period_balance_calculate', 1);
    ledger_period_calculate_balances();
  }

  /**
   * Remove all stored results or stored results up to a certain time for a
   * source.
   */
  public function clear(FeedsSource $source) {

    // Turn off account entry balance calculation temporarily.
    variable_set('ledger_period_balance_calculate', 0);

    // Run the parent process function.
    parent::clear($source);

    // Turn account entry balance calculation back on and recalculate all accounts.
    variable_set('ledger_period_balance_calculate', 1);
    ledger_period_calculate_balances();
  }

  /**
   * Declare default configuration.
   */
  public function configDefaults() {
    $config = parent::configDefaults();

    // Add a default values for the primary and transfer accounts.
    $config['primary_account'] = '';
    $config['transfer_account'] = '';

    return $config;
  }

  /**
   * Overrides parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);

    // Add fields for defining the default credit and debit accounts for imported transactions.
    $form['accounts'] = array(
      '#type' => 'fieldset',
      '#title' => t('Accounts'),
      '#description' => t('If the "Transaction value" mapping target is set, two account entries will be generated for each transaction that is imported. Use the fields below to specify the default accounts to add these account entries to. If no accounts are selected below, all account entries will be added to an account called "Unknown".'),
    );
    $form['accounts']['primary_account'] = array(
      '#type' => 'select',
      '#title' => t('Primary account'),
      '#description' => t('Select the primary account that imported transactions are taking place on. For example, when importing data from an asset account (ie: a bank), select that account here.'),
      '#options' => ledger_account_select_options(),
      '#default_value' => $this->config['primary_account'],
    );
    $form['accounts']['transfer_account'] = array(
      '#type' => 'select',
      '#title' => t('Transfer account'),
      '#description' => t('Select the account that imported transactions are being transferred to/from. For example, when importing data from an asset account (ie: a bank), this would be an expense account to balance the transactions.'),
      '#options' => ledger_account_select_options(),
      '#default_value' => $this->config['transfer_account'],
    );

    return $form;
  }

  /**
   * Declare possible mapping targets that this processor exposes.
   *
   * @ingroup mappingapi
   *
   * @return
   *   An array of mapping targets. Keys are paths to targets
   *   separated by ->, values are TRUE if target can be unique,
   *   FALSE otherwise.
   */
  public function getMappingTargets() {

    // Get default targets from the parent method.
    $targets = parent::getMappingTargets();

    // Add transaction-specific targets.
    $targets += array(
      'aid' => array(
        'name' => t('Transaction ID'),
        'description' => t('The Account ID to assign to the new account. Must be unique.'),
        'optional_unique' => TRUE,
      ),
      'description' => array(
        'name' => t('Description'),
        'description' => t('A description of the transaction.'),
        'optional_unique' => FALSE,
      ),
      'timestamp' => array(
        'name' => t('Transaction timestamp'),
        'description' => t('The timestamp of the transaction.'),
        'optional_unique' => FALSE,
      ),
      'value' => array(
        'name' => t('Transaction value'),
        'description' => t('The value of the transaction.'),
        'optional_unique' => FALSE,
      ),
    );

    // Let other modules expose mapping targets.
    self::loadMappers();
    feeds_alter('feeds_processor_targets', $targets, 'ledger_transaction', 'ledger_transaction');

    // Return the targets.
    return $targets;
  }

  /**
   * Override setTargetElement to operate on a target item that is a transaction.
   */
  public function setTargetElement(FeedsSource $source, $target_item, $target_element, $value) {
    switch ($target_element) {

      // Timestamp (convert to unix timestamp).
      case 'timestamp':
        $target_item->timestamp = feeds_to_unixtime($value, REQUEST_TIME);
        break;

      // Pass anything else to the parent class's setTargetElement().
      default:
        parent::setTargetElement($source, $target_item, $target_element, $value);
        break;
    }
  }
}

