<?php

/**
 * @file
 * OFX transactions feeds importer
 */
$feeds_importer = new stdClass();
$feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
$feeds_importer->api_version = 1;
$feeds_importer->id = 'ledger_import_ofx_transactions';
$feeds_importer->config = array(
  'name' => 'OFX Transaction Import',
  'description' => 'Imports Transactions from an OFX, QFX, or QBO file.',
  'fetcher' => array(
    'plugin_key' => 'FeedsFileFetcher',
    'config' => array(
      'allowed_extensions' => 'ofx qfx qbo',
      'direct' => 0,
    ),
  ),
  'parser' => array(
    'plugin_key' => 'FeedsOFXParser',
    'config' => array(),
  ),
  'processor' => array(
    'plugin_key' => 'FeedsLedgerTransactionProcessor',
    'config' => array(
      'mappings' => array(
        0 => array(
          'source' => 'dtposted',
          'target' => 'timestamp',
          'unique' => FALSE,
        ),
        1 => array(
          'source' => 'fitid',
          'target' => 'guid',
          'unique' => 0,
        ),
        2 => array(
          'source' => 'trnamt',
          'target' => 'value',
          'unique' => FALSE,
        ),
        3 => array(
          'source' => 'Blank source 1',
          'target' => 'description',
          'unique' => FALSE,
        ),
        4 => array(
          'source' => 'trntype',
          'target' => 'Temporary target 1',
          'unique' => FALSE,
        ),
        5 => array(
          'source' => 'name',
          'target' => 'Temporary target 2',
          'unique' => FALSE,
        ),
        6 => array(
          'source' => 'memo',
          'target' => 'Temporary target 3',
          'unique' => FALSE,
        ),
      ),
      'update_existing' => 0,
      'input_format' => NULL,
      'primary_account' => '',
      'transfer_account' => '',
    ),
  ),
  'content_type' => '',
  'update' => 0,
  'import_period' => '-1',
  'expire_period' => 3600,
  'import_on_create' => 1,
  'process_in_background' => 0,
);

